package org.green.kav.jpa.entity.admin.transaction;

import jakarta.persistence.*;
import org.green.kav.jpa.entity.BaseEntity;
import org.green.kav.jpa.entity.admin.reference.RefRoleEntity;

import java.util.UUID;

@Entity
@Table(name = "txn_user_role")
public class UserRoleEntity extends BaseEntity {

    @Id
    @Column(name = "userRoleId", columnDefinition = "UUID", nullable = false)
    private UUID userRoleId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_user", referencedColumnName = "userId")
    private UserEntity user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_role", referencedColumnName = "roleId")
    private RefRoleEntity refRoleEntity;

    public UUID getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(UUID userRoleId) {
        this.userRoleId = userRoleId;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public RefRoleEntity getRefRole() {
        return refRoleEntity;
    }

    public void setRefRole(RefRoleEntity refRoleEntity) {
        this.refRoleEntity = refRoleEntity;
    }
}
