package org.green.kav.jpa.entity.admin.reference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.green.kav.jpa.entity.BaseEntity;

import java.util.UUID;

@Entity
@Table(name = "ref_role")
public class RefRoleEntity extends BaseEntity {

    @Id
    @Column(name = "roleId", columnDefinition = "UUID", nullable = false)
    private UUID roleId;

    @Column(name = "roleName", nullable = false, length = 100, unique = true)
    private String roleName;

    public UUID getRoleId() {
        return roleId;
    }

    public void setRoleId(UUID roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
