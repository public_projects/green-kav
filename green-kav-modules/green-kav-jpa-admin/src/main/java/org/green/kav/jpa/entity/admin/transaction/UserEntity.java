package org.green.kav.jpa.entity.admin.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import org.green.kav.jpa.entity.BaseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "txn_user")
public class UserEntity extends BaseEntity {

    @Id
    @Column(name = "userId", columnDefinition = "UUID", nullable = false)
    private UUID userId;

    @Column(name = "fullName", nullable = false, length = 100, unique = true)
    private String fullname;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "mobileNo", length = 15)
    private String mobileNo;

    @Column(name = "userName", nullable = false, length = 50, unique = true)
    private String userName;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="pwd", nullable = false, length = 25)
    private String pwd;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<UserRoleEntity> userRoleEntities;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Transactional
    public List<UserRoleEntity> getUserRoles() {
        return userRoleEntities;
    }

    public void setUserRoles(List<UserRoleEntity> userRoleEntities) {
        this.userRoleEntities = userRoleEntities;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                       "userId=" + userId +
                       ", fullname='" + fullname + '\'' +
                       ", email='" + email + '\'' +
                       ", mobileNo='" + mobileNo + '\'' +
                       ", userName='" + userName + '\'' +
                       ", pwd='" + pwd + '\'' +
                       '}';
    }
}
