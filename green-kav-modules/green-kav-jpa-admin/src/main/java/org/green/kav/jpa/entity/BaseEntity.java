package org.green.kav.jpa.entity;

import jakarta.persistence.*;
import org.green.kav.jpa.entity.admin.transaction.UserEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity implements Serializable {

    @CreatedBy
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_createdBy")
    private UserEntity createdBy;

    @CreatedDate
    @Column(name = "createdDate", nullable = false, columnDefinition = "timestamp without time zone DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime createdDate;

    @LastModifiedBy
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_updatedBy")
    private UserEntity updatedBy;

    @LastModifiedDate
    @Column(name = "updatedDate", nullable = false, columnDefinition = "timestamp without time zone DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime updatedDate;

    @Version
    @Column(name = "version", columnDefinition = "INT DEFAULT 0")
    private int version;

    @Column(name = "active", columnDefinition = "BIT(1) DEFAULT b'1'", nullable = false)
    private boolean active = true;

    @Column(name = "deleted", columnDefinition = "BIT(1) DEFAULT b'0'", nullable = false)
    private boolean deleted;

    public UserEntity getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserEntity createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public UserEntity getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(UserEntity updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
