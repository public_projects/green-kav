package org.green.kav.jpa.repository.admin.transaction;

import org.green.kav.jpa.entity.admin.transaction.UserRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRoleEntity, UUID> {
}
