package org.green.kav.jpa.repository.admin.reference;

import org.green.kav.jpa.entity.admin.reference.RefRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RefRoleRepository extends JpaRepository<RefRoleEntity, UUID> {

    RefRoleEntity findByRoleName(String role);
}
