package org.green.kav.jpa.repository.admin.transaction;

import org.green.kav.jpa.entity.admin.transaction.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    Optional<UserEntity> findByUserName(String username);

    @Modifying
    @Query("UPDATE UserEntity u SET u.deleted = true WHERE u.userId = :userId")
    int markUserAsDeleted(@Param("userId") UUID userId);
}
