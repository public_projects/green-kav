-- create database jwt_demo_1;

-- Drop all table and recreate them
DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

CREATE TABLE IF NOT EXISTS txn_user
(
    userId       UUID                                   NOT NULL,
    fullName     varchar(100)  NOT NULL,
    email        varchar(100) ,
    mobileNo     varchar(15) ,
    userName     varchar(50)   NOT NULL,
    pwd          varchar(200)   NOT NULL,
    fk_createdBy UUID,
    createdDate  timestamp without time zone    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    fk_updatedBy UUID,
    updatedDate  timestamp without time zone    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    version      int                                         DEFAULT '0',
    active       boolean                             NOT NULL DEFAULT true,
    deleted      boolean                             NOT NULL DEFAULT false,
    PRIMARY KEY (userId),
    CONSTRAINT txn_user_fk1 FOREIGN KEY (fk_createdBy) REFERENCES txn_user (userId) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT txn_user_fk2 FOREIGN KEY (fk_updatedBy) REFERENCES txn_user (userId) ON DELETE NO ACTION ON UPDATE NO ACTION
);

INSERT INTO txn_user (userId, fullName, email, mobileNo, userName, pwd, fk_createdBy, fk_updatedBy, version, active, deleted)
VALUES
    ('ab5b5554-8476-11ee-b962-0242ac120002'::UUID, -- Example UUID for userId
     'Super User',
     'superuser@example.com',
     '123456789',
     'super_user',
     '$2a$12$rGO.bkwCSBw6ju3EY.LzxOc.52KONzcMxZZ3U0qUOvWFTNQA3O7Ce', -- hashed 123456 using https://bcrypt-generator.com/
     NULL, -- fk_createdBy
     NULL, -- fk_updatedBy
     0,    -- version
     true, -- active
     false -- deleted
    );

CREATE TABLE IF NOT EXISTS ref_role
(
    roleId UUID NOT NULL PRIMARY KEY,
    roleName VARCHAR(100)  NOT NULL,
    fk_createdBy UUID DEFAULT NULL,
    createdDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    fk_updatedBy UUID DEFAULT NULL,
    updatedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    version INT DEFAULT '0',
    active BOOLEAN NOT NULL DEFAULT TRUE,
    deleted BOOLEAN NOT NULL DEFAULT FALSE,
    UNIQUE (roleName), -- Assuming roleName should be unique
    CONSTRAINT ref_role_fk1 FOREIGN KEY (fk_createdBy) REFERENCES txn_user (userId) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT ref_role_fk2 FOREIGN KEY (fk_updatedBy) REFERENCES txn_user (userId) ON DELETE NO ACTION ON UPDATE NO ACTION
);

INSERT INTO ref_role (roleId, roleName, fk_createdBy, createdDate, fk_updatedBy, updatedDate, version, active, deleted)
VALUES (
           'd28e085e-84aa-11ee-b962-0242ac120002'::UUID,
           'ROLE_ADMIN',
           'ab5b5554-8476-11ee-b962-0242ac120002'::UUID,
           CURRENT_TIMESTAMP,
           'ab5b5554-8476-11ee-b962-0242ac120002'::UUID,
           CURRENT_TIMESTAMP,
           0,
           true,
           false
       );

CREATE TABLE IF NOT EXISTS txn_user_role
(
    userRoleId   UUID                                    NOT NULL,
    fk_role      UUID                                    NOT NULL,
    fk_user      UUID                                    NOT NULL,
    fk_createdBy UUID                                             DEFAULT NULL,
    createdDate  timestamp without time zone              NOT NULL DEFAULT CURRENT_TIMESTAMP,
    fk_updatedBy UUID                                             DEFAULT NULL,
    updatedDate  timestamp without time zone              NOT NULL DEFAULT CURRENT_TIMESTAMP,
    version      integer                                          DEFAULT '0',
    active       boolean                                  NOT NULL DEFAULT TRUE,
    deleted      boolean                                  NOT NULL DEFAULT FALSE,
    PRIMARY KEY (userRoleId),
    CONSTRAINT txn_user_role_fk1 FOREIGN KEY (fk_createdBy) REFERENCES txn_user (userId) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT txn_user_role_fk2 FOREIGN KEY (fk_updatedBy) REFERENCES txn_user (userId) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT txn_user_role_fk3 FOREIGN KEY (fk_role) REFERENCES ref_role (roleId) ON DELETE NO ACTION ON UPDATE NO ACTION
);

INSERT INTO txn_user_role (
    userRoleId,
    fk_role,
    fk_user,
    fk_createdBy,
    createdDate,
    fk_updatedBy,
    updatedDate,
    version,
    active,
    deleted
)
VALUES (
           -- Use x::uuid for explicit casting of UUID values
           '0b3bf77903ca497a95df7181ab79607b'::uuid,
           'd28e085e84aa11eeb9620242ac120002'::uuid,
           'ab5b5554847611eeb9620242ac120002'::uuid,
           'ab5b5554847611eeb9620242ac120002'::uuid,
           DEFAULT,  -- Use DEFAULT for CURRENT_TIMESTAMP
           'ab5b5554847611eeb9620242ac120002'::uuid,
           DEFAULT,  -- Use DEFAULT for CURRENT_TIMESTAMP
           0,
           TRUE,
           FALSE
       );
