package org.green.kav.jpa;

import org.green.kav.jpa.entity.admin.reference.RefRoleEntity;
import org.green.kav.jpa.entity.admin.transaction.UserEntity;
import org.green.kav.jpa.entity.admin.transaction.UserRoleEntity;
import org.green.kav.jpa.repository.admin.reference.RefRoleRepository;
import org.green.kav.jpa.repository.admin.transaction.UserRepository;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
        classes = TestConfiguration.class)
class GreenKavJpaSecurityApplicationTests {
    private Logger logger = LoggerFactory.getLogger(GreenKavJpaSecurityApplicationTests.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RefRoleRepository refRoleRepository;

    @Test
    @Transactional
    void contextLoads() {
        assertEquals(1, userRepository.count());

        Optional<UserEntity> userEntity = userRepository.findById(UUID.fromString("ab5b5554-8476-11ee-b962-0242ac120002"));
        assertTrue(userEntity.isPresent());

        List<UserRoleEntity> userRoleEntities = userEntity.get().getUserRoles();
        UserRoleEntity userRoleEntity = userRoleEntities.getFirst();
        assertNotNull(userRoleEntity);

        RefRoleEntity refRole = userRoleEntity.getRefRole();
        assertNotNull(refRole);

        List<RefRoleEntity> refRoleEntities  = refRoleRepository.findAll();
        assertNotNull(refRoleEntities);

        assertNotNull(refRoleEntities.getFirst());

    }

}
