package org.green.kav.service.admin.expection;

public class AdminException extends Exception {
    public AdminException(String s) {
        super(s);
    }
}
