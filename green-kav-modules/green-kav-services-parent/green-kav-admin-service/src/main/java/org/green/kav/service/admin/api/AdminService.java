package org.green.kav.service.admin.api;

import org.green.kav.model.admin.reference.RefRoleModel;
import org.green.kav.model.admin.transaction.UserModel;
import org.green.kav.service.admin.expection.AdminException;

import java.util.List;

public interface AdminService {
    List<UserModel> getAllUsers();

    List<RefRoleModel> getAllUserRoleRef();

    UserModel register(UserModel userModel) throws AdminException;
}
