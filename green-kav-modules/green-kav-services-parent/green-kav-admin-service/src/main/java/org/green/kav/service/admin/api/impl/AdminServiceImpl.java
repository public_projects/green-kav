package org.green.kav.service.admin.api.impl;

import org.green.kav.jpa.entity.admin.reference.RefRoleEntity;
import org.green.kav.jpa.entity.admin.transaction.UserEntity;
import org.green.kav.jpa.repository.admin.reference.RefRoleRepository;
import org.green.kav.jpa.repository.admin.transaction.UserRepository;
import org.green.kav.model.admin.reference.RefRoleModel;
import org.green.kav.model.admin.transaction.UserModel;
import org.green.kav.service.admin.api.AdminService;
import org.green.kav.service.admin.expection.AdminException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AdminServiceImpl implements AdminService {

    private UserRepository userRepository;

    private RefRoleRepository refRoleRepository;

    @Autowired
    public AdminServiceImpl(UserRepository userRepository, RefRoleRepository refRoleRepository) {
        this.userRepository = userRepository;
        this.refRoleRepository = refRoleRepository;
    }

    @Override
    public List<UserModel> getAllUsers() {
        return this.userRepository.findAll()
                .stream().map(this::toUserModel)
                .collect(Collectors.toList());
    }

    @Override
    public List<RefRoleModel> getAllUserRoleRef() {
        return this.refRoleRepository.findAll()
                       .stream().map(this::toRoleModel)
                       .collect(Collectors.toList());
    }

    @Override
    public UserModel register(UserModel user) throws AdminException {
        String username = user.getUserName();
        if (isUserNameTaken(username)) throw new AdminException("User with username=" + username + ", already exist");
        UserEntity newUser = toUserEntity(user);
        UserEntity userEntity = userRepository.save(newUser);
        return toUserModel(userEntity);
    }

    boolean isUserNameTaken(String username) {
        Optional<UserEntity> userEntity =userRepository.findByUserName(username);
        return userEntity.isPresent();
    }

    private UserEntity toUserEntity(UserModel userModel) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(UUID.randomUUID());
        userEntity.setEmail(userModel.getEmail());
        userEntity.setUserName(userModel.getUserName());
        userEntity.setFullname(userModel.getFullname());
        userEntity.setMobileNo(userModel.getMobileNo());
        userEntity.setPwd(encodePassword("Test123"));
        LocalDateTime ldt = LocalDateTime.now();
        userEntity.setCreatedDate(ldt);
        userEntity.setUpdatedDate(ldt);
        return userEntity;
    }

    private static String encodePassword(String password) {
        // Encode password using Base64
        byte[] encodedBytes = Base64.getEncoder().encode(password.getBytes());
        return new String(encodedBytes);
    }

    private UserModel toUserModel(UserEntity user) {
        UserModel userModel = new UserModel();
        userModel.setUserId(user.getUserId());
        userModel.setEmail(user.getEmail());
        userModel.setUserName(user.getUserName());
        userModel.setFullname(user.getFullname());
        userModel.setMobileNo(user.getMobileNo());
        return userModel;
    }

    private RefRoleModel toRoleModel(RefRoleEntity refRole) {
        RefRoleModel roleModel = new RefRoleModel();
        roleModel.setRoleId(refRole.getRoleId());
        roleModel.setRoleName(refRole.getRoleName());
        return roleModel;
    }
}
