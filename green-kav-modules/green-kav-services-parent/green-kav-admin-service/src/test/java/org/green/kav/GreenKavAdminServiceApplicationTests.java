package org.green.kav;

import org.green.kav.service.admin.api.AdminService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
		classes = TestConfiguration.class)
class GreenKavAdminServiceApplicationTests {

	@Autowired
	private AdminService adminService;

	@Test
	void contextLoads() {
		Assertions.assertEquals(1, adminService.getAllUsers().size());
	}

}
