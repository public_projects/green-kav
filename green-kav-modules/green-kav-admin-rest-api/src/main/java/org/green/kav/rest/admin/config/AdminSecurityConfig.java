package org.green.kav.rest.admin.config;

import static org.green.kav.rest.admin.AdminApiEndpointCatalog.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class AdminSecurityConfig {
    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
//        permitAll(http);
        authenticate(http);
        return http.build();
    }

    private void permitAll(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(authorizationManagerRequestMatcherRegistry -> authorizationManagerRequestMatcherRegistry.anyRequest().permitAll());
        http.formLogin(httpSecurityFormLoginConfigurer -> httpSecurityFormLoginConfigurer.disable()); // Disable form-based login
        http.httpBasic(httpSecurityHttpBasicConfigurer -> httpSecurityHttpBasicConfigurer.disable()); // Disable HTTP Basic authentication
        http.csrf(csrfConfigurer -> csrfConfigurer.disable());
    }

    private void authenticate(HttpSecurity http) throws Exception {
        http.csrf(csrfConfigurer -> csrfConfigurer.disable());
        
        http.authorizeHttpRequests((requests) -> {
            requests.requestMatchers(BASE_ADMIN_URI + GET_ALL_USERS).authenticated();
            requests.requestMatchers(BASE_ADMIN_URI + GET_ALL_ROLES).permitAll();
            requests.requestMatchers("/admin/v1/register").permitAll();
        });
        http.formLogin(Customizer.withDefaults());
        http.httpBasic(Customizer.withDefaults());
    }
}
