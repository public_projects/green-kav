package org.green.kav.rest.admin;

public interface AdminApiEndpointCatalog {

    String BASE_ADMIN_URI = "/admin/v1";
    String GET_ALL_ROLES = "/all/roles";
    String GET_ALL_USERS = "/all/users";
    String REGISTER_USER = "/register";
}
