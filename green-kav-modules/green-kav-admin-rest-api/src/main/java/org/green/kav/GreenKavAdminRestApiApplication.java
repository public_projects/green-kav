package org.green.kav;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreenKavAdminRestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreenKavAdminRestApiApplication.class, args);
    }

}

