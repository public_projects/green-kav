package org.green.kav.rest.admin.controller;

import org.green.kav.model.admin.reference.RefRoleModel;
import org.green.kav.model.admin.transaction.UserModel;
import org.green.kav.rest.admin.AdminApiEndpointCatalog;
import org.green.kav.service.admin.api.AdminService;
import org.green.kav.service.admin.expection.AdminException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AdminApiEndpointCatalog.BASE_ADMIN_URI)
public class AdminController {
    private AdminService adminService;

    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping(AdminApiEndpointCatalog.GET_ALL_USERS)
    public ResponseEntity<List<UserModel>> getAllUsers() {
        List<UserModel> userModels = adminService.getAllUsers();
        return ResponseEntity.ok(userModels);
    }

    @GetMapping(AdminApiEndpointCatalog.GET_ALL_ROLES)
    public ResponseEntity<List<RefRoleModel>> getAllRoles() {
        List<RefRoleModel> refRoleModels = adminService.getAllUserRoleRef();
        return ResponseEntity.ok(refRoleModels);
    }

    @PostMapping(value = AdminApiEndpointCatalog.REGISTER_USER)
    public ResponseEntity<UserModel> register(@RequestBody UserModel inputModel) {
        UserModel userModel;
        try {
            userModel = adminService.register(inputModel);
        } catch (AdminException e) {
            return ResponseEntity.of(ProblemDetail.forStatusAndDetail(HttpStatusCode.valueOf(500), "Failed")).build();
        }
        return ResponseEntity.ok(userModel);
    }

}
