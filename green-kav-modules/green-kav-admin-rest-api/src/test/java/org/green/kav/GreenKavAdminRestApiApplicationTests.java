package org.green.kav;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * This is to keep the test configuration separate from the main application. Even if the main application don't have @SpringBootApplication,
 * this unit test could run independently.
 *
 * @see https://www.baeldung.com/spring-boot-testing
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(
		classes = TestConfiguration.class)
class GreenKavAdminRestApiApplicationTests {

	@Test
	void contextLoads() {
	}

}
