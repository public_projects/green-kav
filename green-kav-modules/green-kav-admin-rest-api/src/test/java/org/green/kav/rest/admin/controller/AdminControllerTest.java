package org.green.kav.rest.admin.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.green.kav.model.admin.transaction.UserModel;
import org.green.kav.service.admin.api.AdminService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = AdminController.class)
class AdminControllerTest {

    @MockBean
    private AdminService adminService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        UserModel user1 = new UserModel();
        user1.setMobileNo("1");
        user1.setUserName("TestUser1");

        UserModel user2 = new UserModel();
        user2.setMobileNo("2");
        user2.setUserName("TestUser2");

        List<UserModel> users = Arrays.asList(user1, user2);

        Mockito.when(adminService.getAllUsers()).thenReturn(users);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllUsers() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                                                      .get("/admin/v1/all/users")
                                                      .with(SecurityMockMvcRequestPostProcessors.httpBasic("admin", "admin"))
                                                      .contentType(MediaType.APPLICATION_JSON))
                                      .andExpect(status().isOk())
                                      .andReturn();

        String json = mvcResult.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        List<UserModel> foundUsers = mapper.readValue(json, new TypeReference<>() {});

        Assertions.assertEquals(2, foundUsers.size());
    }
}