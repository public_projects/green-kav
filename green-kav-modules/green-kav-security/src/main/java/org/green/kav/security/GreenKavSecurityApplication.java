package org.green.kav.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreenKavSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreenKavSecurityApplication.class, args);
	}

}
